<?php

/**
 * @file
 * Handles XML-related stuff for Commerce Fastway Couriers module.
 */

/**
 * This builds the array of attributes to submit to Fastway Couriers for rates.
 *
 * @param object $order
 *   stdClass of order to be rated.
 *
 * @return array
 *   Array containing the attributes that will be sent to Fastway API
 */
function commerce_fastway_couriers_build_rate_request(stdClass $order) {

  drupal_load('module', 'packaging');
  module_load_include('inc', 'packaging', 'classes/package');
  module_load_include('inc', 'packaging', 'classes/product');
  module_load_include('inc', 'packaging', 'classes/context');
  module_load_include('inc', 'packaging', 'classes/strategy');
  module_load_include('inc', 'commerce_fastway_couriers', 'strategies/commerce_fastway_couriers_last_fit_2d');
  module_load_include('inc', 'packaging', 'strategies/package_each_in_own');
  module_load_include('inc', 'packaging', 'strategies/package_all_in_one');
  module_load_include('inc', 'packaging', 'strategies/package_last_fit');
  module_load_include('inc', 'packaging', 'strategies/package_next_fit');
  module_load_include('inc', 'packaging', 'strategies/package_one_package');
  module_load_include('inc', 'packaging', 'strategies/package_average_volume');
  module_load_include('inc', 'packaging', 'strategies/package_by_volume');
  module_load_include('inc', 'packaging', 'strategies/package_average_weight');
  module_load_include('inc', 'packaging', 'strategies/package_by_key');
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $package_dimensions = array(
    'length' => variable_get('commerce_fastway_couriers_default_package_length', '0'),
    'width' => variable_get('commerce_fastway_couriers_default_package_width', '0'),
    'height' => variable_get('commerce_fastway_couriers_default_package_height', '0'),
  );
  $default_package_volume = $package_dimensions['length'] *
                            $package_dimensions['width'] *
                            $package_dimensions['height'];

  // Determine the shipping profile reference field name for the order.
  $context = new PackagingContext();
  $context->setMaximumPackageWeight(25);
  $context->setMaximumPackageVolume($default_package_volume);
  $context->setDefaultWeightUnits('KG');
  $context->setDefaultLengthUnits('CM');
  $strategy = packaging_get_instance(variable_get('commerce_fastway_couriers_packaging_strategy', 'allinone'));
  $context->setStrategy($strategy);
  $products = array();
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item_weight = commerce_physical_product_line_item_weight($line_item_wrapper->value());
    $line_item_dimensions = commerce_physical_product_line_item_dimensions($line_item_wrapper->value());
    $product = new PackagingProduct();
    $product->type = $line_item_wrapper->type->value();
    $product->model = $line_item_wrapper->line_item_label->value();
    $product->setPrice((int) ($line_item_wrapper->commerce_unit_price->amount->value()));
    $product->setWeight($line_item_weight['weight'] / $line_item_wrapper->quantity->value());
    $product->setWeightUnits(strtoupper($line_item_weight['unit']));
    $product->setDimensions($line_item_dimensions);
    $product->setLengthUnits(strtoupper($line_item_dimensions['unit']));
    $product->setPackageQuantity(1);
    $product->setQuantity((int) ($line_item_wrapper->quantity->value()));
    $product->shippable = TRUE;
    $products[] = $product;
  }
  $packages = packaging_package_products($context, $products);

  if (!count($packages)) {
    return FALSE;
  }

  $number_of_packages = count($packages);

  // Ship To - Customer Shipping Address.
  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->commerce_customer_shipping->commerce_customer_address)) {
    $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
  }
  foreach ($packages as $package) {
    $attributes[] = array(
      'country_code' => check_plain($shipping_address['country']),
      'RFCode'       => check_plain(variable_get('commerce_fastway_couriers_rfcode')),
      'DestPostcode' => check_plain($shipping_address['postal_code']),
      'Suburb'       => check_plain($shipping_address['locality']),
      'params'     => array(
        'LengthInCm' => $package_dimensions['length'],
        'WidthInCm'  => $package_dimensions['width'],
        'HeightInCm' => $package_dimensions['height'],
        'WeightInKg' => $package->getWeight(),
      ),
    );
  }
  return array('packages' => $number_of_packages, 'attributes' => $attributes);
}

/**
 * Submits an API request to Fastway's Price service calculator.
 *
 * @param array $attributes
 *   Array containing the packages and attributes that will be sent to
 *   the Fastway API.
 *
 * @return array
 *   Returns XML array containing the shipping rate
 */
function commerce_fastway_couriers_api_request(array $attributes) {
  $attributes['params']['api_key'] = variable_get('commerce_fastway_couriers_api_key');
  $path = $attributes['RFCode'] . '/' . str_replace(' ', '%20', $attributes['Suburb']) . '/' . $attributes['DestPostcode'] . '.xml';

  if (variable_get('commerce_fastway_couriers_shipping_type') == 'weight') {
    unset($attributes['params']['LengthInCm']);
    unset($attributes['params']['WidthInCm']);
    unset($attributes['params']['HeightInCm']);
  }

  $url = url('http://au.api.fastway.org/v4/psc/lookup/' . $path, array('query' => $attributes['params']));
  $logging = variable_get('commerce_fastway_couriers_log', array());
  if (isset($logging['request']) && $logging['request']) {
    $message = t('Submitting API request to Fastway Couriers');
    watchdog('fastway',
             '@message:<pre>@url</pre>',
              array(
                '@message' => $message,
                '@url' => print_r($url, TRUE),
              )
            );
  }
  $result = drupal_http_request($url);
  if (!empty($result)) {
    if (isset($logging['response']) && $logging['response']) {
      // Arrange the XML response into an XML tree suitable for display to log.
      $dom = new DOMDocument('1.0');
      $dom->preserveWhiteSpace = FALSE;
      $dom->formatOutput = TRUE;
      $dom->loadXML($result->data);
      $xmltree = $dom->saveXML();
      // Log the API request if specified.
      watchdog('fastway',
               'API response received:<pre>@xml</pre>', array('@xml' => $xmltree));
    }
  }
  // If we received data back from the server...
  if (!empty($result)) {
    return $result;
  }
  else {
    return FALSE;
  }
}
