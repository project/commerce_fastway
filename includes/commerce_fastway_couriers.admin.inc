<?php

/**
 * @file
 * Handles admin settings page for Commerce Fastway module.
 */

/**
 * Implements hook_settings_form().
 */
function commerce_fastway_couriers_settings_form($form, &$form_state) {
  drupal_load('module', 'packaging');

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fastway API credentials'),
    '#collapsible' => TRUE,
    '#description' => t('In order to obtain shipping rate estimates, you must have an account with Fastway. You can apply for Fastway API credentials at !Fastway',
                      array(
                        '!Fastway' => l(t('api.fastway.org'),
                        'http://au.api.fastway.org/v4/docs/page/GetAPIKey.html',
                        array(
                          'attributes' => array('target' => '_blank'),
                        )
                        ),
                      )
    ),
  );

  $form['api']['commerce_fastway_couriers_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Fastway API Key'),
    '#default_value' => variable_get('commerce_fastway_couriers_api_key'),
    '#size' => 35,
    '#required' => TRUE,
  );

  $form['origin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ship From Franchise'),
    '#collapsible' => TRUE,
  );

  $form['origin']['commerce_fastway_couriers_rfcode'] = array(
    '#type' => 'select',
    '#title' => t('Select Local Franchise'),
    '#options' => _commerce_fastway_couriers_franchise_codes(),
    '#default_value' => variable_get('commerce_fastway_couriers_rfcode', 'SYD'),
  );

  $form['labels'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select the labels to be used'),
    '#collapsible' => TRUE,
  );

  $header = array(
    'color' => t('color'),
    'price' => t('label price'),
  );

  $label_values = array();
  $labels = _commerce_fastway_couriers_service_list();
  $label_keys = array_keys(_commerce_fastway_couriers_service_list());
  $label_init = array_fill_keys($label_keys, FALSE);
  $price_init = array_fill_keys($label_keys, '0.00');
  $label_values = variable_get('commerce_fastway_couriers_label_price', $price_init);
  $options = array();

  foreach ($label_keys as $label_color) {
    $options[$label_color] = array(
      'color' => $labels[$label_color]['title'],
      'price' => array(
        'data' => array(
          '#id' => $label_color,
          '#type' => 'textfield',
          '#title' => check_plain($label_color) . ' price',
          '#title_display' => 'invisible',
          '#name' => 'commerce_fastway_couriers_label_price[' . $label_color . ']',
          '#value' => $label_values[$label_color],
          '#size' => 7,
          '#tree' => TRUE,
        ),
      ),
    );
  }

  $form['labels']['commerce_fastway_couriers_label_colors'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => TRUE,
    // '#size' => 500px,
    '#default_value' => variable_get('commerce_fastway_couriers_label_colors', $label_init),
  );

  $form['labels']['commerce_fastway_couriers_label_price'] = array(
    '#type' => 'value',
    '#default_value' => variable_get('commerce_fastway_couriers_label_price'),
  );

  // Fields for default package size (cm).
  $form['default_package_size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default maximum package size (cm)'),
    '#collapsible' => FALSE,
    '#description' => t('Fastway Couriers requires a package size when determining estimates.'),
  );
  $form['default_package_size']['commerce_fastway_couriers_default_package_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Length'),
    '#size' => 5,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_fastway_couriers_default_package_length'),
  );
  $form['default_package_size']['commerce_fastway_couriers_default_package_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 5,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_fastway_couriers_default_package_width'),
  );
  $form['default_package_size']['commerce_fastway_couriers_default_package_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#size' => 5,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_fastway_couriers_default_package_height'),
  );
  $form['default_package_size']['commerce_fastway_couriers_default_package'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use variable package sizes up to maximum size'),
    '#default_value' => variable_get('commerce_fastway_couriers_default_package', 0),
  );
  $form['default_package_size']['details'] = array(
    '#markup' => '<strong>Fastway Couriers will not be available to items with no dimensions or weight.</strong>',
  );

  // Use CTools to look for any modules which define packaging strategies.
  $operations = packaging_get_strategies();
  $options = array();
  foreach ($operations as $id => $operation) {
    $options[$id] = $operation['title'];
  }

  // Form to select packaging strategy.
  $form['default_package_size']['commerce_fastway_couriers_packaging_strategy'] = array(
    '#type'          => 'select',
    '#title'         => t('Packaging strategy'),
    '#default_value' => variable_get('commerce_fastway_couriers_packaging_strategy', ''),
    '#options'       => $options,
    '#description'   => t('Select the packaging strategy that is most appropriate to the types of products you sell.'),
  );

  // Fields for default package size (cm).
  $form['advanced_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced_options']['commerce_fastway_couriers_show_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Fastway Couriers Logo on Shipping Page'),
    '#default_value' => variable_get('commerce_fastway_couriers_show_logo', 0),
  );
  $form['advanced_options']['commerce_fastway_couriers_show_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Fastway Couriers delivery time in addition to title on the Shipping Service selection pane'),
    '#default_value' => variable_get('commerce_fastway_couriers_show_description', 0),
  );
  $form['advanced_options']['commerce_fastway_couriers_log'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => variable_get('commerce_fastway_couriers_log', array()),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function commerce_fastway_couriers_settings_form_validate($form, &$form_state) {
  $label_keys = array_keys(_commerce_fastway_couriers_service_list());
  if (!isset($label_keys)) {
    $label_keys = array_fill_keys($label_keys, '0.00');
  }
  $label_selected = FALSE;

  foreach ($label_keys as $label_color) {
    if ($form_state['values']['commerce_fastway_couriers_label_price'][$label_color] != '') {
      if (!preg_match("/^-?[0-9]+(?:\.[0-9]{1,2})?$/", ($form_state['values']['commerce_fastway_couriers_label_price'][$label_color]))) {
        form_set_error('', t('Please enter valid label pricing.'));
      }
    }
    if (!$form_state['values']['commerce_fastway_couriers_label_colors'][$label_color] == 0) {
      $label_selected = TRUE;
    }
  }
  if ($label_selected == FALSE) {
    form_set_error('', t('Please select at least 1 label.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function commerce_fastway_couriers_settings_form_submit($form, &$form_state) {

  $services = variable_get('commerce_fastway_couriers_label_colors', NULL);

  // If the selected services have changed then rebuild caches.
  if ($services !== $form_state['values']['commerce_fastway_couriers_label_colors']) {
    variable_set('commerce_fastway_couriers_label_colors', $form_state['values']['commerce_fastway_couriers_label_colors']);

    commerce_shipping_services_reset();
    entity_defaults_rebuild();
    rules_clear_cache(TRUE);
    menu_rebuild();
  }
  variable_set('commerce_fastway_couriers_api_key', $form_state['values']['commerce_fastway_couriers_api_key']);
  variable_set('commerce_fastway_couriers_rfcode', $form_state['values']['commerce_fastway_couriers_rfcode']);
  variable_set('commerce_fastway_couriers_default_package_length', $form_state['values']['commerce_fastway_couriers_default_package_length']);
  variable_set('commerce_fastway_couriers_default_package_width', $form_state['values']['commerce_fastway_couriers_default_package_width']);
  variable_set('commerce_fastway_couriers_default_package_height', $form_state['values']['commerce_fastway_couriers_default_package_height']);
  variable_set('commerce_fastway_couriers_default_package', $form_state['values']['commerce_fastway_couriers_default_package']);
  variable_set('commerce_fastway_couriers_label_price', $form_state['values']['commerce_fastway_couriers_label_price']);
  variable_set('commerce_fastway_couriers_show_description', $form_state['values']['commerce_fastway_couriers_show_description']);
  variable_set('commerce_fastway_couriers_log', $form_state['values']['commerce_fastway_couriers_log']);
  variable_set('commerce_fastway_couriers_show_logo', $form_state['values']['commerce_fastway_couriers_show_logo']);
  variable_set('commerce_fastway_couriers_packaging_strategy', $form_state['values']['commerce_fastway_couriers_packaging_strategy']);

  drupal_set_message(t('The Fastway Couriers configuration options have been saved.'));
}
