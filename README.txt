
-- SUMMARY --

Integrates Fastway Couriers Price Service Calculator with Commerce Shipping.
  
For a full description of the module, visit the project page:
  http://drupal.org/sandbox/jhesketh/1851514

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1851514


-- REQUIREMENTS --

commerce
http://drupal.org/project/commerce

commerce_shipping (7.x-2.x)
http://drupal.org/project/commerce_shipping

commerce_physical
http://drupal.org/project/commerce_physical

This module requires an API key with Fastway Couriers in order to access their
shipping calculations. 
You can apply for one free here:
http://au.api.fastway.org/v4/docs/page/GetAPIKey.html

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Go to Store -> Shipping -> Shipping Methods
* Edit the Fastway Couriers Method
  * Put in your Fastway Couriers API key
  * Select your local Fastway franchise
  * Select the shipping methods you wish to make available in your store
* Go to Store -> Products -> Product Types -> Edit your product type
  * Add physical dimensions and physical weight to your product
  * Update your available products with physical details
* Done! (See your shipping checkout page).

-- CONTACT --

Current maintainers:
* Craig Herbert - http://drupal.org/user/3209007

This project has been sponsored by:
* SimCentral - http://www.simcentral.com.au/
